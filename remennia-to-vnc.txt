https://wiki.ingeg.it/notes/remmina-vnc-raspberry

Connect to Raspberry Pi OS via VNC using Remmina

– Tested with Raspberry Pi OS on July 2020 –

When trying to connect from your Linux machine using Remmina to a Raspberry Pi running Raspberry PI OS with RealVNC enabled you get the error

Unknown authentication scheme from VNC server: 13, 5, 6, 130, 192

RealVNC only supports a few security schemes. Authentication=VncAuth seems to be the only scheme that allows direct connections from VNC-compatible Viewer projects from third parties. In order to change to VncAuth scheme in your Raspbian and set a password to accept connections from Remmina VNC plugin, open a SSH session (or a terminal window) on the Raspberry and generate your VNC password with:

sudo vncpasswd -service

Now, edit the file /root/.vnc/config.d/vncserver-x11

sudo nano /root/.vnc/config.d/vncserver-x11

and add the following line at the end of the file:

Authentication=VncAuth

Now your config file should look more or less like mine:

_AnlLastConnTime=int64:0000000000000000
_LastUpdateCheckSuccessTime=int64:01d65c12272dff1a
_LastUpdateCheckTime=int64:01d65c12272dff1a
Password=c3abbea3b003a0b231737c0541892d72
Authentication=VncAuth

c3abbea3b003a0b231737c0541892d72 is the encrypted version of raspberry; your line will be different.

Eventually, restart the VNC server service with

sudo systemctl restart vncserver-x11-serviced

and you are ready to connect to you Raspberry Pi using Remmina.
