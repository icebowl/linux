ssh to another machine with no password
my local ip is 192.168.1.8
Test ssh first:
ssh user@192.168.1.7

generate a key
ssh-keygen -t rsa -b 4096

keep default leave passphrase blank

 move id_rsa.pub to other machine

make sure server has a ~/.ssh directory
#copy key to server
#scp ~/.ssh/id_rsa.pub username@192.168.1.7:/home/user/.ssh/uploaded_key.pub
cd ~/.ssh/
scp id_rsa.pub username@192.168.1.7:/home/user/.ssh/uploaded_key.pub
ON SERVER AUTHENICATE THE KEY

cat ~/.ssh/uploaded_key.pub >> ~/.ssh/authorized_keys
change permissions
#chmod 700 ~/.ssh/
#chmod 600 ~/.ssh/*

Now test 
cwc@192.168.1.7
scp files cwc@192.168.1.7:~/
